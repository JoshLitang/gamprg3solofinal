﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Used for the match's clock and timing
public class GameTimer : MonoBehaviour
{
    //Variables
    private float sysTime;
    private float currentSeconds;
    private int currentMinute;

    private void Start()
    {
        Time.timeScale = 1.0f;
    }

    private void Update()
    {
        Clock();
        DebugFastForward();
    }

    //The game's clocking counting up from 00:00
    private void Clock()
    {
        //Makes sure delta time is scaled to 0 to 60;
        sysTime += Time.deltaTime;
        sysTime %= 60f;

        //counting Seconds
        if(sysTime >= 1)
        {
            currentSeconds++;
            sysTime = 0;
        }

        //counting Minutes
        if(currentSeconds >= 60)
        {
            currentMinute++;
            currentSeconds = 0;
        }
    }

    //returns the current minute and seconds of the game
    public Vector2 GetTime()
    {
        Vector2 time;
        time = new Vector2(currentMinute, currentSeconds);
        return time;
    }

    //Holding the 'L' key will fast forward the gametime
    void DebugFastForward()
    {
        if (Mathf.Sign(Input.GetAxis("DebugGameSpeed")) == -1)
        {
            Time.timeScale = 90f;
        }
        else if (Mathf.Sign(Input.GetAxis("DebugGameSpeed")) == 1)
        {
            Time.timeScale = 1.0f;
        }
    }
}
