﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    protected GameObject Victim;
    protected GameObject Shooter;

    public float Speed;
    public string ID;

    public virtual void OnLaunched()
    {

    }

    public virtual void InAir()
    {

    }

    public virtual void OnContact()
    {

    }

    protected virtual void DestroySelf(float countdown)
    {
        Destroy(gameObject, countdown);
    }
}
