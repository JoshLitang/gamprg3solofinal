﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Creep : MonoBehaviour
{
    public CreepTraits Traits;
    public List<Transform> Waypoints;
    [SerializeField]
    public Transform AttackPoint;
    public Collider[] Radar;
    
    private NavMeshAgent navigator;
    [SerializeField]
    private LayerMask sightLayerMasks;
    private Animator anim;
    private int WyPntCache;
    private int wpIndex;
    private GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        navigator = GetComponent<NavMeshAgent>();
        navigator.speed = Traits.MovementSpeed;
        Movement();
        wpIndex = 0;
        anim.SetFloat("Speed", 1);
        StartCoroutine("SwitchDestination");
        StartCoroutine("SwitchDestination");
    }

    //Dealing with physics detection so this is FixedUpdate is preferable
    private void FixedUpdate()
    {
        //debug destroy creeps because right now they'r not working properly
        if (wpIndex == 4)
        {
            StartCoroutine("OnDeath");
        }
    }

    private void Movement()
    {
        if (target == null) {
            navigator.SetDestination(Waypoints[wpIndex].transform.position);
        }

        else if(target != null) {
            Attack(target);
            if (Vector3.Distance(transform.position, target.transform.position) > Traits.AttackRange) {
                target = null;
                navigator.SetDestination(Waypoints[WyPntCache].transform.position);
            }
        }
        if (Vector3.Distance(transform.position, Waypoints[wpIndex].transform.position) < 3.0f && target == null) {
            WyPntCache = wpIndex;
            wpIndex++;
            navigator.SetDestination(Waypoints[wpIndex].transform.position);
        }
    }

    private void Attack(GameObject enemy)
    {
        navigator.SetDestination(enemy.transform.position);
        
        anim.SetTrigger("Attack");
        Collider[] hitTarget = Physics.OverlapSphere(AttackPoint.position, Traits.AttackRange, sightLayerMasks);

        foreach(Collider opponent in hitTarget)
        {
            Debug.Log("hit confirmed " + opponent.name);
            anim.ResetTrigger("Attack");
        }
    }

    private void Sighting() {
        Collider[] sightTarget = Physics.OverlapSphere(transform.position, Traits.SightRange, sightLayerMasks);

        foreach (Collider opponent in sightTarget) {
            
            if (opponent.gameObject.tag == "Creep") {

            }

            else if (opponent.gameObject.tag == "Tower") {
                Debug.Log("Go Attack");
                target = opponent.gameObject;
            }
        }
    }

    //delayed loop for switching waypoints
    private IEnumerator SwitchDestination() {
        while (true) {
            Sighting();
            Movement();
            yield return new WaitForSeconds(1f);
        }
        
    }

    //Draws sight range and attack range
    private void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(AttackPoint.position, Traits.AttackRange);
        Gizmos.DrawWireSphere(transform.position, Traits.SightRange);
    }

    private IEnumerator OnDeath() {

        anim.Play("Death");
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
}
