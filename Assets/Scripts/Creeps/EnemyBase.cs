﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ActionState
{
    FollowPath,
    Chase, 
    Attack,
    ReturnToPoint
}

public enum EnemyType
{
    MeleeCreep,
    RangeCreep,
    SiegeCreep,
    MegaCreep
}

public class EnemyBase : MonoBehaviour
{
    [Header("Character Information")]
    public float maxHealth;
    public float currentHealth;
    public EnemyType type;
    public ActionState enemyState;
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
