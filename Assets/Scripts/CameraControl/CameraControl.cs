﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    //Variables
    private float scrollSPeed = 30f;
    private float scrollDistance = 50f;
    private Vector3 mousePos;
    private Vector3 pos;
    private int borderWidth;
    private int borderHeight;

    //To adjust the points where the mouse triggers movement on the screen
    [SerializeField]
    private int borderBuffer = 30;

    private void Start()
    {
        //caching cam position and adjusting 
        pos = transform.position;
        borderWidth = Screen.width - borderBuffer;
        borderHeight = Screen.height - borderBuffer;
    }

    // Update is called once per frame
    void Update()
    {
        ZoomCam();
        MoveCam();
    }

    private void MoveCam()
    {
        mousePos = Input.mousePosition;
        float moveSpeed = scrollSPeed * Time.deltaTime;
        
        //Depending if the mouse is at one edge or a corner of the screen the camera will move
        if (mousePos.y >= borderHeight)
        {
            pos += Vector3.right * moveSpeed;
        }

        else if (mousePos.y <= borderBuffer)
        {
            pos += Vector3.left * moveSpeed;
        }

        if (mousePos.x >= borderWidth)
        {
            pos += Vector3.back * moveSpeed;
        }

        else if (mousePos.x <= borderBuffer)
        {
            pos += Vector3.forward * moveSpeed;
        }

        transform.position = pos;
    }

    //Scrolling up will zoom out and scrolling down will soom out
    private void ZoomCam()
    {
        float moveSpeed = scrollDistance * Time.deltaTime;
        if (Mathf.Sign(Input.mouseScrollDelta.y) == 1 && Input.mouseScrollDelta.y != 0)
        {
            pos += Vector3.down * moveSpeed;
        }

        else if (Mathf.Sign(Input.mouseScrollDelta.y) == -1)
        {
            pos += Vector3.up * moveSpeed;
        }

        transform.position = pos;
    }
}
