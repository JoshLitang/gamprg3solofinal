﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Tier
{
    T1,
    T2,
    T3,
    T4
};

public class Towers : MonoBehaviour
{
    //References
    public Tier TowerTier;
    public List<Towers> BackDoorPrtctr;
    public Collider[] Radar;
    public GameObject AttackPrjctl;
    public GameObject Turret;
    public GameObject Target;
    public BuildingStats Stats;
    public LayerMask layermasks;

    private GameObject shot;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Fire");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        //Targeting();

    }

    //Detect and assign target
    private void Targeting()
    {
        if (Target != null) {
            if (Vector3.Distance(transform.position, Target.transform.position) > Stats.AttackRange)
            {
                Target = null;
            }
        }
        Collider[] Radar = Physics.OverlapSphere(transform.position, Stats.AttackRange, layermasks);

        foreach (Collider opponent in Radar)
        {
            if (opponent.gameObject.tag == "Creep" && Target == null)
            {
                Target = opponent.gameObject;
                Debug.Log(opponent.gameObject.name);
            }
        }
    }

    //After getting a target the tower will fire
    private IEnumerator Fire()
    {
        if (Target != null)
        {
            Debug.Log("Shot");
            TowerProjectile tracker;

            shot = Instantiate(AttackPrjctl, Turret.transform.position, Quaternion.identity);

            tracker = shot.gameObject.GetComponent<TowerProjectile>();
            tracker.Target = Target.transform;
            //if (Vector3.Distance(transform.position, Target.transform.position) > Stats.AttackRange)
            //{
            //    Target = null;
            //    Debug.Log("Target Decouple works");
            //}
        }

        yield return new WaitForSeconds(Stats.BasicAttackTime);
    }
    
    //For destroying all Towers on button press
    private void DebugSelfDestroy()
    {

    }

    //To visualize the Tower's attack/sight range
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, Stats.AttackRange);
    }
}
