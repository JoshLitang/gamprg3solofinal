﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "BuildingStats", menuName = "Scriptables/Building Stats", order = 2)]
public class BuildingStats : ScriptableObject
{
    //pluggable specs for buildings
    public int Health;
    public int Armor;
    public int Damage;
    public float BasicAttackTime;
    public float AttackRange;
    public float ProjectileSpeed;
    public int TeamGoldBounty;
    public int LastHitGBounty;
    public int TPAB;
    public float HPRegen;
    public float MagicRes;
}
