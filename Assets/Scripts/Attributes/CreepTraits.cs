﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "CreepTraits", menuName = "Scriptables/Creep Traits", order = 0)]
public class CreepTraits : ScriptableObject
{
    //Specs for Creeps
    public int HealthPoints;
    public int GoldBounty;
    public int ExpBounty;
    public int AttackDamage;
    public float AttackRange;
    //plugged into the navmesh agent
    public float MovementSpeed;
    //How far they can detect players and other AI
    public float SightRange;
}
