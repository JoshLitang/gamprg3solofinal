﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team
{
    Dire,
    Radiance
};

public class Faction : MonoBehaviour
{
    public Team Side;
}
